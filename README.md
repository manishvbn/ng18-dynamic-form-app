# Dynamic Forms in Angular18

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 18.0.6 and uses Angular Standalone Components

This project is an Angular application that dynamically generates forms based on user selection. It utilizes Angular's reactive forms and integrates Bootstrap and Ng Bootstrap for styling. The application includes an in-memory web API for development purposes.

## Authors

- **Manish Sharma -** 
_Entrepreneur, Tech Enthusiast, Corporate Trainer, Consultant, Founder Technizer India_
- [Technizer India](https://www.technizerindia.com/)
- [LinkedIn](https://www.linkedin.com/in/manishsharma30/)

## Table of Contents

- [Installation](#installation)
- [Run](#run)
- [Usage](#usage)
- [Features](#features)
- [Development](#development)
- [License](#license)

## Installation

To get started with this project, follow these steps:

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/manishvbn/ng18-dynamic-form-app
   ```
   
2. **Install:**

    ```bash
    cd ng18-dynamic-form-app
    npm install
    ```

## Run

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

   ```bash
   ng serve
   ```

## Usage
- Select a user from the dropdown menu.
- The form fields corresponding to the selected user will be loaded dynamically.
- Fill out the form fields and submit. The data enetered in the form will be visible in **browser console**.
- If you do not submit with proper input, you will see validation errors on the UI

## Features
- Dynamic Form Generation: Forms are generated based on the selected user, with fields and validation rules dynamically applied.
- Bootstrap Integration: Utilizes Bootstrap & ng-bootstrap components for responsive and consistent styling.
- In-Memory Web API: Uses angular-in-memory-web-api for simulating backend API during development.
- Validation: Includes field-level validation with error messages.

**Conditional Imports for Development and Production**
- The application conditionally uses InMemoryDataService during development and skips it in production. This is configured based on the environment settings.
- **Development Configuration**: src/environments/environment.development.ts
- **Production Configuration**: src/environments/environment.ts

## Development

**Adding New Form Fields:**

- Update the In-Memory Data Service:
- In src/app/services/in-memory-data.service.ts, add the new form fields to the appropriate user configuration.

   ```typescript
    const userSpecificFields = [
      {
        id: 'user1',
        fields: [
          {
            name: 'email',
            type: 'email',
            label: 'Email',
            value: '',
            validators: {
              required: true,
              email: true
            }
          },
          // Add new fields here
        ]
      },
      // Other Users
    ]
    ```

**Ensure Field Handling in Template:**

- Make sure the template src/app/components/dynamic-form/dynamic-form.component.html can handle the new field types, if necessary.

## License
This project is licensed under the MIT License
