import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { DynamicFormService } from '../../services/dynamic-form.service';
import { FormField, FormConfig } from '../../models/form-config.model';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css'],
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, NgbModule, FormsModule]
})
export class DynamicFormComponent implements OnInit {
  dynamicForm: FormGroup;
  userId: string = '';
  formConfig: FormConfig | null = null;
  controlOrder: string[] = [];
  users: { id: string, name: string }[] = [
    { id: 'user1', name: 'User One' },
    { id: 'user2', name: 'User Two' }
  ];
  isLoading: boolean = false;

  constructor(private fb: FormBuilder, private dynamicFormService: DynamicFormService, private titleService: Title) {
    this.dynamicForm = this.fb.group({});
  }

  ngOnInit(): void {
    this.titleService.setTitle('Dynamic Form | Manish Sharma');
  }

  onUserChange(): void {
    if (this.userId) {
      this.isLoading = true;
      this.dynamicFormService.getFormConfig(this.userId).subscribe({
        next: config => {
          this.formConfig = config;
          this.createForm(config);
          this.isLoading = false;
        },
        error: err => {
          console.error('Error fetching form configuration', err);
          this.isLoading = false;
        }
      });
    } else {
      this.dynamicForm = this.fb.group({});
      this.controlOrder = [];
    }
  }

  createForm(formConfig: FormConfig): void {
    this.dynamicForm = this.fb.group({}); // Reset the form
    this.controlOrder = []; // Reset the control order

    formConfig.fields.forEach((field: FormField) => {
      const control = this.fb.control(
        field.value || '',
        this.mapValidators(field.validators)
      );
      this.dynamicForm.addControl(field.name, control);
      this.controlOrder.push(field.name); // Keep track of the control order
    });
  }

  mapValidators(validators: any): Validators | null {
    if (validators) {
      const formValidators = [];
      for (const validator of Object.keys(validators)) {
        switch (validator) {
          case 'required':
            if (validators.required) {
              formValidators.push(Validators.required);
            }
            break;
          case 'minLength':
            formValidators.push(Validators.minLength(validators.minLength));
            break;
          case 'maxLength':
            formValidators.push(Validators.maxLength(validators.maxLength));
            break;
          case 'min':
            formValidators.push(Validators.min(validators.min));
            break;
          case 'max':
            formValidators.push(Validators.max(validators.max));
            break;
          case 'email':
            if (validators.email) {
              formValidators.push(Validators.email);
            }
            break;
          // Add other validators as needed
        }
      }
      return Validators.compose(formValidators);
    }
    return null;
  }

  getFieldType(fieldName: string): string {
    const fieldConfig = this.getFieldConfig(fieldName);
    return fieldConfig ? fieldConfig.type : 'text';
  }

  getOptions(fieldName: string): string[] {
    const fieldConfig = this.getFieldConfig(fieldName);
    return fieldConfig && fieldConfig.options ? fieldConfig.options : [];
  }

  getFieldConfig(fieldName: string): FormField | undefined {
    return this.formConfig ? this.formConfig.fields.find(field => field.name === fieldName) : undefined;
  }

  getOrderedControls(): { [key: string]: any } {
    const orderedControls: { [key: string]: any } = {};
    this.controlOrder.forEach((controlName) => {
      orderedControls[controlName] = this.dynamicForm.controls[controlName];
    });
    return orderedControls;
  }

  onSubmit(): void {
    if (this.dynamicForm.valid)
      console.log(this.dynamicForm.value);
    else
      this.dynamicForm.markAllAsTouched();
  }
}
