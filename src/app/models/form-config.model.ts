export interface ValidatorConfig {
    required?: boolean;
    minLength?: number;
    maxLength?: number;
    min?: number;
    max?: number;
    email?: boolean;
}

export interface FormField {
    name: string;
    type: string;
    label: string;
    value: string | number | boolean;
    options?: string[];  // Make sure options is defined as an optional property
    validators?: ValidatorConfig;
}

export interface FormConfig {
    fields: FormField[];
}
