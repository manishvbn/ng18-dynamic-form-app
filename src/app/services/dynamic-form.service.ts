import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, forkJoin, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { FormField, FormConfig } from '../models/form-config.model';

@Injectable({
  providedIn: 'root'
})
export class DynamicFormService {
  private commonFieldsUrl = 'api/commonFields/';
  private userSpecificFieldsUrl = 'api/userSpecificFields/';

  constructor(private http: HttpClient) { }

  getFormConfig(userId: string): Observable<{ fields: FormField[] }> {
    return forkJoin({
      common: this.http.get<FormField[]>(this.commonFieldsUrl),
      specific: this.http.get<{ id: string, fields: FormField[] }[]>(`${this.userSpecificFieldsUrl}`)
        .pipe(
          map(userFields => userFields.find(u => u.id === userId)?.fields || [])
        )
    }).pipe(
      retry(2),
      map(result => ({
        fields: [...result.common, ...result.specific]
      })),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(() => new Error(error.statusText));
      })
    );
  }
}
