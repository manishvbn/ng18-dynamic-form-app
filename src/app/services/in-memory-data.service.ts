import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { FormField } from '../models/form-config.model';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const commonFields: FormField[] = [
      {
        name: 'firstName',
        type: 'text',
        label: 'First Name',
        value: '',
        validators: {
          required: true,
          minLength: 3
        }
      },
      {
        name: 'lastName',
        type: 'text',
        label: 'Last Name',
        value: '',
        validators: {
          required: true,
          minLength: 3
        }
      },
      {
        name: 'gender',
        type: 'radio',
        label: 'Gender',
        options: ['Male', 'Female', 'Other'],
        value: '',
        validators: {
          required: true
        }
      },
      {
        name: 'country',
        type: 'dropdown',
        label: 'Country',
        options: ['India', 'USA', 'Canada', 'Australia'],
        value: '',
        validators: {
          required: true
        }
      }
    ];

    const userSpecificFields = [
      {
        id: 'user1',
        fields: [
          {
            name: 'email',
            type: 'email',
            label: 'Email',
            value: '',
            validators: {
              required: true,
              email: true
            }
          },
          {
            name: 'age',
            type: 'number',
            label: 'Age',
            value: '',
            validators: {
              required: true,
              min: 18
            }
          }
        ]
      },
      {
        id: 'user2',
        fields: [
          {
            name: 'mobile',
            type: 'text',
            label: 'Mobile Number',
            value: '',
            validators: {
              required: true,
              minLength: 10,
              maxLength: 10
            }
          },
          {
            name: 'password',
            type: 'password',
            label: 'Password',
            value: '',
            validators: {
              required: true,
              minLength: 8
            }
          }
        ]
      }
    ];


    return { commonFields, userSpecificFields };
  }
}
